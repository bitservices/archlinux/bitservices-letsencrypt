#!/bin/bash -e
###############################################################################

set -o pipefail

###############################################################################

hash systemctl

###############################################################################

if [ -z "${1}" ]; then exit 1; fi

###############################################################################

SERVICE_NAMES=(${1})

###############################################################################

for SERVICE_NAME in "${SERVICE_NAMES[@]}"; do
  if [ -n "${SERVICE_NAME}" ]; then
    if systemctl is-active --quiet "${SERVICE_NAME}.service"; then
      echo "Sending update command to ${SERVICE_NAME} service..."
      systemctl reload "${SERVICE_NAME}.service"
    fi
  fi
done

###############################################################################
