# Maintainer: Richard Lees <git0@bitservices.io>
###############################################################################

pkgname=bitservices-letsencrypt
pkgver=0.0.5
pkgrel=4
pkgdesc="BITServices Let's Encrypt Helper Scripts and Services"
arch=('any')
license=('none')
depends=('acl' 'aws-cli' 'certbot' 'certbot-dns-route53' 'openssl')
source=("${pkgname}-account.sh"
        "${pkgname}-bounce.sh"
        "${pkgname}-install.service"
        "${pkgname}-install.sh"
        "${pkgname}-install.timer"
        "${pkgname}-renew.service"
        "${pkgname}-renew.sh"
        "${pkgname}-renew.timer"
        "${pkgname}-ssm-install.sh"
        "${pkgname}-ssm-install-retry.service"
        "${pkgname}-ssm-install-retry.sh"
        "${pkgname}-ssm-install-retry.timer"
        "${pkgname}-valid.sh")
sha256sums=('74ac33bda66fd630c289b51a7cdbf9d728a6327404bfdf969b13c92c7e466cef'
            'c36356d2734f1fd74dad9ed87ad3c4e0214e98ee78aadcd9105fdcae350554a7'
            '7f434dba23e101a3962f4c335508e161fb5a9599f999e7e5b75788035bbb80ea'
            'd8dd7776673b1138ca5a4ae1f8760c4f746d64f2dd4803c0e6d8c4c98d41f492'
            '85baff8fdc7996c224d03bb262ada6e6c57467590209bd70349c4f1014fb1f40'
            '70497506f3723fa5e2e877299c54cb6de67a26105031198fd7e692de8ef50862'
            '3a26501fb7061717343bfcafcdb4fb5cd25080959f44227d74082ef94230f717'
            'c73e79ffcb6ab118d2412cd23de4e986336f59b3630d74c80c1fc0546592954b'
            '9b31f48b07c1776b06088e2f0076636b88cecac5563bc42556f3c41270063690'
            'b5d0d54315cddd4de21b58335cdc136ab8eefaa84620e59be29dd05511d16e12'
            '8ffa9b77e19ecf7e8e37cbaf434fccb1b4b28c96485160e8cb72e3b62fa3f790'
            '7216153316c1be0e7282e35341a26bb1fdb8b7de68ebeda4aa4512ecc3ed6af6'
            '189b74b14214591be22b65b781b5db81e06813769e0f9fe78511ab2fd092f87e')

###############################################################################

package() {
  install -d "${pkgdir}/usr/bin"
  install -d "${pkgdir}/usr/lib/systemd/system"

  install -m755 "${srcdir}/${pkgname}-account.sh" "${pkgdir}/usr/bin/${pkgname}-account"
  install -m755 "${srcdir}/${pkgname}-bounce.sh" "${pkgdir}/usr/bin/${pkgname}-bounce"
  install -m755 "${srcdir}/${pkgname}-install.sh" "${pkgdir}/usr/bin/${pkgname}-install"
  install -m755 "${srcdir}/${pkgname}-renew.sh" "${pkgdir}/usr/bin/${pkgname}-renew"
  install -m755 "${srcdir}/${pkgname}-ssm-install.sh" "${pkgdir}/usr/bin/${pkgname}-ssm-install"
  install -m755 "${srcdir}/${pkgname}-ssm-install-retry.sh" "${pkgdir}/usr/bin/${pkgname}-ssm-install-retry"
  install -m755 "${srcdir}/${pkgname}-valid.sh" "${pkgdir}/usr/bin/${pkgname}-valid"

  install -m644 "${srcdir}/${pkgname}-install.service" "${pkgdir}/usr/lib/systemd/system/${pkgname}-install.service"
  install -m644 "${srcdir}/${pkgname}-install.timer" "${pkgdir}/usr/lib/systemd/system/${pkgname}-install.timer"
  install -m644 "${srcdir}/${pkgname}-renew.service" "${pkgdir}/usr/lib/systemd/system/${pkgname}-renew.service"
  install -m644 "${srcdir}/${pkgname}-renew.timer" "${pkgdir}/usr/lib/systemd/system/${pkgname}-renew.timer"
  install -m644 "${srcdir}/${pkgname}-ssm-install-retry.service" "${pkgdir}/usr/lib/systemd/system/${pkgname}-ssm-install-retry.service"
  install -m644 "${srcdir}/${pkgname}-ssm-install-retry.timer" "${pkgdir}/usr/lib/systemd/system/${pkgname}-ssm-install-retry.timer"
}

###############################################################################
