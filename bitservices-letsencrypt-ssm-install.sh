#!/bin/bash -e
###############################################################################

set -o pipefail

###############################################################################

hash mv
hash aws
hash echo
hash chmod
hash chown
hash setfacl
hash sha256sum
hash bitservices-letsencrypt-valid

###############################################################################

if [   -z "${CONFIG}"      ]; then exit 1; fi
if [   -z "${REGION}"      ]; then exit 1; fi
if [   -z "${SERVICE}"     ]; then exit 1; fi
if [   -z "${CERT_ROOT}"   ]; then exit 1; fi
if [ ! -d "${CERT_ROOT}"   ]; then exit 1; fi
if [   -z "${APPLICATION}" ]; then exit 1; fi

###############################################################################

echo "Getting ${APPLICATION} certificate..."
aws ssm get-parameter --name "/${SERVICE}/${CONFIG}/${APPLICATION}-certificate" --region "${REGION}" --with-decryption --query "Parameter.Value" --output text > "${CERT_ROOT}/certificate.crt.tmp"
chmod 400 "${CERT_ROOT}/certificate.crt.tmp"

echo "Getting ${APPLICATION} key..."
aws ssm get-parameter --name "/${SERVICE}/${CONFIG}/${APPLICATION}-key" --region "${REGION}" --with-decryption --query "Parameter.Value" --output text > "${CERT_ROOT}/key.pem.tmp"
chmod 400 "${CERT_ROOT}/key.pem.tmp"

[ -s "${CERT_ROOT}/certificate.crt.tmp" ] && [ -s "${CERT_ROOT}/key.pem.tmp" ]
bitservices-letsencrypt-valid "${CERT_ROOT}/certificate.crt.tmp" "${CERT_ROOT}/key.pem.tmp"

echo "Certificate found and still valid! Installing locally..."
mv -fv "${CERT_ROOT}/certificate.crt.tmp" "${CERT_ROOT}/certificate.crt"
mv -fv "${CERT_ROOT}/key.pem.tmp" "${CERT_ROOT}/key.pem"

if [ -n "${CERT_USERS}" ]; then
  CERT_USERS_ARRAY=($CERT_USERS)

  for CERT_USER in "${CERT_USERS_ARRAY[@]}"; do
    if [ -n "${CERT_USER}" ]; then
      setfacl -m "u:${CERT_USER}:r" "${CERT_ROOT}/certificate.crt"
      setfacl -m "u:${CERT_USER}:r" "${CERT_ROOT}/key.pem"
    fi
  done
fi

###############################################################################

if [ -n "${CERT_SERVICES}" ]; then
  exec bitservices-letsencrypt-bounce "${CERT_SERVICES}"
fi

###############################################################################
