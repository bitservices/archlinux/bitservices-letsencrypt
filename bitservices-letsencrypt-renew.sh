#!/bin/bash -e
###############################################################################

set -o pipefail

###############################################################################

hash echo
hash certbot
hash bitservices-letsencrypt-valid

###############################################################################

if [   -z "${DOMAIN}"    ]; then exit 1; fi
if [   -z "${CERT_ROOT}" ]; then exit 1; fi
if [ ! -d "${CERT_ROOT}" ]; then exit 1; fi

###############################################################################

if [ -s "${CERT_ROOT}/certificate.crt" ] && [ -s "${CERT_ROOT}/key.pem" ] && bitservices-letsencrypt-valid "${CERT_ROOT}/certificate.crt" "${CERT_ROOT}/key.pem"; then
  echo "Certificate is present and valid! Skipping renewal..."
else
  echo "Attempting to acquire/renew certificate for: ${DOMAIN}..."
  certbot certonly --dns-route53 -d "${DOMAIN}"
fi

###############################################################################
