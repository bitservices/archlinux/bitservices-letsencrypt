#!/bin/bash -e
###############################################################################

set -o pipefail

###############################################################################

hash bitservices-letsencrypt-valid
hash bitservices-letsencrypt-ssm-install

###############################################################################

if [   -z "${CONFIG}"      ]; then exit 1; fi
if [   -z "${REGION}"      ]; then exit 1; fi
if [   -z "${SERVICE}"     ]; then exit 1; fi
if [   -z "${CERT_ROOT}"   ]; then exit 1; fi
if [ ! -d "${CERT_ROOT}"   ]; then exit 1; fi
if [   -z "${APPLICATION}" ]; then exit 1; fi

###############################################################################

BACKOFF_LIMIT=${BACKOFF_LIMIT:-5}
BACKOFF_MULTI=${BACKOFF_MULTI:-60}

###############################################################################

if [ -s "${CERT_ROOT}/certificate.crt" ] && [ -s "${CERT_ROOT}/key.pem" ] && bitservices-letsencrypt-valid "${CERT_ROOT}/certificate.crt" "${CERT_ROOT}/key.pem"; then exit 0; fi

###############################################################################

for (( ATTEMPT=1; ATTEMPT<=${BACKOFF_LIMIT}; ATTEMPT++ )); do
  bitservices-letsencrypt-ssm-install && exit 0 || true
  WAIT=$(( ${ATTEMPT} * ${BACKOFF_MULTI} ))
  echo "Backoff re-try for downloading a valid certificate, attempt: ${ATTEMPT}, wait: ${WAIT} seconds"
  sleep ${WAIT}
done

echo "Giving up getting a valid certificate!"
exit 1

###############################################################################
