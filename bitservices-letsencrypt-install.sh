#!/bin/bash -e
###############################################################################

set -o pipefail

###############################################################################

hash cp
hash aws
hash echo
hash chown
hash chmod
hash setfacl
hash sha256sum
hash bitservices-letsencrypt-valid
hash bitservices-letsencrypt-bounce

###############################################################################

if [   -z "${OWNER}"        ]; then exit 1; fi
if [   -z "${CONFIG}"       ]; then exit 1; fi
if [   -z "${DOMAIN}"       ]; then exit 1; fi
if [   -z "${REGION}"       ]; then exit 1; fi
if [   -z "${COMPANY}"      ]; then exit 1; fi
if [   -z "${KMS_KEY}"      ]; then exit 1; fi
if [   -z "${SERVICE}"      ]; then exit 1; fi
if [   -z "${CERT_ROOT}"    ]; then exit 1; fi
if [ ! -d "${CERT_ROOT}"    ]; then exit 1; fi
if [   -z "${APPLICATION}"  ]; then exit 1; fi
if [   -z "${CERTBOT_ROOT}" ]; then exit 1; fi
if [ ! -d "${CERTBOT_ROOT}" ]; then exit 1; fi

###############################################################################

if [ -s "${CERT_ROOT}/certificate.crt" ] && [ -s "${CERT_ROOT}/key.pem" ] && bitservices-letsencrypt-valid "${CERT_ROOT}/certificate.crt" "${CERT_ROOT}/key.pem"; then

###############################################################################

  echo "Certificate is present and valid! Skipping installation..."

###############################################################################

else

###############################################################################

  echo "Attempting to install certificate for: ${DOMAIN}..."
  bitservices-letsencrypt-valid "${CERTBOT_ROOT}/live/${DOMAIN}/fullchain.pem" "${CERTBOT_ROOT}/live/${DOMAIN}/privkey.pem"

###############################################################################

  echo "Uploading ${APPLICATION} certificate..."
  aws ssm put-parameter --name "/${SERVICE}/${CONFIG}/${APPLICATION}-certificate" --region "${REGION}" --tier "Advanced" --type SecureString --key-id "${KMS_KEY}" --overwrite --value "$(cat "${CERTBOT_ROOT}/live/${DOMAIN}/fullchain.pem")"
  aws ssm add-tags-to-resource \
    --region "${REGION}" \
    --resource-type "Parameter" \
    --resource-id "/${SERVICE}/${CONFIG}/${APPLICATION}-certificate" \
    --tags "[{\"Key\":\"Name\", \"Value\":\"/${SERVICE}/${CONFIG}/${APPLICATION}-certificate\"},{\"Key\":\"Class\", \"Value\":\"${APPLICATION}-certificate\"},{\"Key\":\"Service\", \"Value\":\"${SERVICE}\"},{\"Key\":\"Config\", \"Value\":\"${CONFIG}\"},{\"Key\":\"Region\", \"Value\":\"${REGION}\"},{\"Key\":\"Owner\", \"Value\":\"${OWNER}\"},{\"Key\":\"Company\", \"Value\":\"${COMPANY}\"}]"

  echo "Uploading ${APPLICATION} key..."
  aws ssm put-parameter --name "/${SERVICE}/${CONFIG}/${APPLICATION}-key" --region "${REGION}" --type SecureString --key-id "${KMS_KEY}" --overwrite --value "$(cat "${CERTBOT_ROOT}/live/${DOMAIN}/privkey.pem")"
  aws ssm add-tags-to-resource \
    --region "${REGION}" \
    --resource-type "Parameter" \
    --resource-id "/${SERVICE}/${CONFIG}/${APPLICATION}-key" \
    --tags "[{\"Key\":\"Name\", \"Value\":\"/${SERVICE}/${CONFIG}/${APPLICATION}-key\"},{\"Key\":\"Class\", \"Value\":\"${APPLICATION}-key\"},{\"Key\":\"Service\", \"Value\":\"${SERVICE}\"},{\"Key\":\"Config\", \"Value\":\"${CONFIG}\"},{\"Key\":\"Region\", \"Value\":\"${REGION}\"},{\"Key\":\"Owner\", \"Value\":\"${OWNER}\"},{\"Key\":\"Company\", \"Value\":\"${COMPANY}\"}]"

###############################################################################

  echo "Installing certificates locally..."
  cp -fv "${CERTBOT_ROOT}/live/${DOMAIN}/fullchain.pem" "${CERT_ROOT}/certificate.crt"
  cp -fv "${CERTBOT_ROOT}/live/${DOMAIN}/privkey.pem" "${CERT_ROOT}/key.pem"
  chmod 400 "${CERT_ROOT}/certificate.crt"
  chmod 400 "${CERT_ROOT}/key.pem"

###############################################################################

  if [ -n "${CERT_USERS}" ]; then
    CERT_USERS_ARRAY=($CERT_USERS)

    for CERT_USER in "${CERT_USERS_ARRAY[@]}"; do
      if [ -n "${CERT_USER}" ]; then
        setfacl -m "u:${CERT_USER}:r" "${CERT_ROOT}/certificate.crt"
        setfacl -m "u:${CERT_USER}:r" "${CERT_ROOT}/key.pem"
      fi
    done
  fi

###############################################################################

  if [ -n "${CERT_SERVICES}" ]; then
    exec bitservices-letsencrypt-bounce "${CERT_SERVICES}"
  fi

###############################################################################

fi

###############################################################################
