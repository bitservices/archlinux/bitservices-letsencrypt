#!/bin/bash -e
###############################################################################

set -o pipefail

###############################################################################

hash openssl

###############################################################################

if [   -z "${1}" ]; then exit 1; fi
if [ ! -f "${1}" ]; then exit 1; fi
if [   -z "${2}" ]; then exit 1; fi
if [ ! -f "${2}" ]; then exit 1; fi

###############################################################################

CERTIFICATE_MINIMUM_SECONDS=${CERTIFICATE_MINIMUM_SECONDS:-691200}

###############################################################################

echo "Checking validity of: ${1}"

###############################################################################

openssl x509 -checkend ${CERTIFICATE_MINIMUM_SECONDS} -noout -in "${1}"

###############################################################################

echo "Checking validity of: ${2}"

###############################################################################

openssl pkey -check -noout -in "${2}"

###############################################################################

echo "Checking ${1} and ${2} match"

###############################################################################

CERT_HASH="$(openssl x509 -pubkey -noout -in "${1}")"
KEY_HASH="$(openssl pkey -pubout -in "${2}")"

[ -n "${CERT_HASH}" ]
[ -n "${KEY_HASH}" ]

[ "${CERT_HASH}" == "${KEY_HASH}" ]

###############################################################################
