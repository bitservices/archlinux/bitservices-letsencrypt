#!/bin/bash -e
###############################################################################

set -o pipefail

###############################################################################

hash aws
hash echo
hash chmod
hash mkdir

###############################################################################

if [   -z "${CONFIG}"       ]; then exit 1; fi
if [   -z "${REGION}"       ]; then exit 1; fi
if [   -z "${SERVICE}"      ]; then exit 1; fi
if [   -z "${CERTBOT_ROOT}" ]; then exit 1; fi
if [ ! -d "${CERTBOT_ROOT}" ]; then exit 1; fi

###############################################################################

echo "Getting ACME server..."
ACME_SERVER="$(aws ssm get-parameter --name "/${SERVICE}/${CONFIG}/acme-server" --region "${REGION}" --with-decryption --query "Parameter.Value" --output text)"
if [ -z "${ACME_SERVER}" ]; then exit 1; fi

echo "Getting ACME account ID..."
ACME_ACCOUNT_ID="$(aws ssm get-parameter --name "/${SERVICE}/${CONFIG}/acme-account-id" --region "${REGION}" --with-decryption --query "Parameter.Value" --output text)"
if [ -z "${ACME_ACCOUNT_ID}" ]; then exit 1; fi

CERTBOT_ACCOUNT_ROOT="${CERTBOT_ROOT}/accounts"
CERTBOT_ACCOUNT="${CERTBOT_ACCOUNT_ROOT}/${ACME_SERVER}/directory/${ACME_ACCOUNT_ID}"

echo "Creating CertBot account folders..."
mkdir -p "${CERTBOT_ACCOUNT}"

echo "Getting ACME metadata..."
aws ssm get-parameter --name "/${SERVICE}/${CONFIG}/acme-metadata" --region "${REGION}" --with-decryption --query "Parameter.Value" --output text > "${CERTBOT_ACCOUNT}/meta.json"
if [ ! -s "${CERTBOT_ACCOUNT}/meta.json" ]; then exit 1; fi

echo "Getting ACME registration..."
aws ssm get-parameter --name "/${SERVICE}/${CONFIG}/acme-registration" --region "${REGION}" --with-decryption --query "Parameter.Value" --output text > "${CERTBOT_ACCOUNT}/regr.json"
if [ ! -s "${CERTBOT_ACCOUNT}/regr.json" ]; then exit 1; fi

echo "Getting ACME private key..."
aws ssm get-parameter --name "/${SERVICE}/${CONFIG}/acme-private-key" --region "${REGION}" --with-decryption --query "Parameter.Value" --output text > "${CERTBOT_ACCOUNT}/private_key.json"
if [ ! -s "${CERTBOT_ACCOUNT}/private_key.json" ]; then exit 1; fi

echo "Setting CertBot account folder permissions..."
chmod -R u+rwX,go-rwx "${CERTBOT_ACCOUNT_ROOT}"

echo "CertBot account restored!"

###############################################################################
